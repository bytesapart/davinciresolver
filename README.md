# DaVinciResolver

DaVinci Resolver is a great free alternative to Adobe Premiere on Linux. However, it is restricted because it does not support a lot of codecs on linux.
This non-support of codecs means one cannot use H.264 codec to record videos, which means that more CPU cycles are consumed while recording videos while
the GPU sits ideal, since NVIDIA supports using the GPU to record in H.264 codec using nvenc. This helps in using an ideal GPU while recording stuff such as 
Tutorials where the GPU usage is non existant, while CPU is used to a very large degree.

Therefore, in order to use videos recorded using NVIDIA's nvenc codec, we need to make a pipline of converting our recording (*from, say, OBS) to a support codec
within DaVinci Resolve (both Audio and Video) and then, post the editing performed in DaVinci Resolve, we need to convert it back to the original library's codec.

